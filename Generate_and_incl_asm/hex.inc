	.file	"hex.c"
	.text
	.code16gcc
#	.globl	toHex
	.type	toHex, @function
toHex:
.LFB0:
#	.cfi_startproc
	pushl	%ebp
#	.cfi_def_cfa_offset 8
#	.cfi_offset 5, -8
	movl	%esp, %ebp
#	.cfi_def_cfa_register 5
	subl	$20, %esp
	movl	8(%ebp), %eax
	movb	%al, -20(%ebp)
	movb	-20(%ebp), %al
	andl	$15, %eax
	movb	%al, -1(%ebp)
	movb	-20(%ebp), %al
	shrb	$4, %al
	movb	%al, -2(%ebp)
	cmpb	$9, -1(%ebp)
	ja	.L2
	addb	$48, -1(%ebp)
	jmp	.L3
.L2:
	addb	$87, -1(%ebp)
.L3:
	cmpb	$9, -2(%ebp)
	ja	.L4
	addb	$48, -2(%ebp)
	jmp	.L5
.L4:
	addb	$87, -2(%ebp)
.L5:
	xorl	%eax, %eax
	movb	-2(%ebp), %al
	movw	%ax, -4(%ebp)
	salw	$8, -4(%ebp)
	xorl	%eax, %eax
	movb	-1(%ebp), %al
	orw	%ax, -4(%ebp)
	movl	-4(%ebp), %eax
	leave
#	.cfi_restore 5
#	.cfi_def_cfa 4, 4
	ret
#	.cfi_endproc
.LFE0:
	.size	toHex, .-toHex
	.ident	"GCC: (GNU) 4.6.2"
