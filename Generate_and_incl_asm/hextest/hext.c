#include <stdio.h>

unsigned short cfunc(unsigned char c) {
	unsigned short word;
	unsigned char low;
	unsigned char high;
	low = c%16;
	high = c/16;
	if( low < 10 )
		low += '0';
	else
		low += 'a'-10;
	if( high < 10 )
		high += '0';
	else
		high += 'a'-10;
	word = high;
	word <<= 8;
	word |= low;
	return word;
}

int main() {

	char a = 0xf4;
	unsigned short w;
	w = cfunc(a);
	a = (char)w;
	printf("0x%c", a);
	a = w>>8;
	printf("%c\n", a);

return 0;
}
