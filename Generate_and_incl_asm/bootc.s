#keyboard input
.text
.code16

jmp _start

.include "print.inc"
.include "hex.inc"

.code16

_start:
	#set the data segment address
	xorw %ax, %ax
	movw %ax, %ds

	#setup stack
	#past loaded 512 bytes
	movw %ax, %ss
	movw $0x9c00, %sp

	movb $0xaf, %al
	#movb $0x68, %ah

	pushl %eax

.code16gcc
	call toHex
.code16

	popl %ebx

	xchgb %al, %ah

	call printw

	# /r
	movb $0xd, %al
	# /n
	movb $0xa, %ah

	call printw

hang:
	jmp hang

.align 256, 0
.fill 254, 1, 0
.byte 0x55
.byte 0xAA



