#vga test
.code16
  jmp start

.include "print.inc"
#.include "hex.inc"
#.include "printw.inc"
.code16

start:
	xorw %ax, %ax
	movw %ax, %ds	
	movw %ax, %ss
	movw $0x9c00, %sp			#stack

	#a
#	movb $0x61, %al
#	call print

	#set video mode
	xorb %ah, %ah	#always 0
	movb $0x13, %al	#mode: 0x13 graphic, 3 text
	int $0x10

	#render a pixel with BIOS
#	movb $0x0c, %ah
#	movb $0xff, %al	#color 0x0f white
#	movw $160, %cx		#x
#	movw $100, %dx		#y
#	int $0x10

#by writing to memory:
movw $0xA000, %ax		#video memory offset
movw %ax, %es
movw $32010, %ax			#middle of the scr: 32010?
movw %ax, %di
movb $0x0f, %dl		#7 gray, 16 white
movb %dl, %es:(%di)


loop:
  jmp loop


.align 256, 0
.fill 254, 1, 0
.word 0xAA55

