.text
.code16
#parameter passed on stack
#print character on lowest address
print:
	pushw %ax
	movw %sp, %si
	lodsb
	movb $0x0E,%ah
	int $0x10
	popw %ax
	ret
