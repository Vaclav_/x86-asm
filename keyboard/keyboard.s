#keyboard.s
#prints the scancode of the keyboard key hit down in hexadecimal
.code16
#[ORG 0x7c00]      ; add to offsets?
  jmp start

.include "printw.inc"
.include "hex.inc"
.code16

start:

	xorw %ax,% ax   #zero
	movw %ax, %ds
	movw %ax, %ss
	movw $0x9c00, %sp   #200h past code start
	movw $0xb800, %ax   #text video memory
	movw %ax, %es

	#set up keyboard interrupt
	cli
	movw $0x0009, %bx   #hardware interrupt no.
	shlw $2, %bx   #multiply by 4
	xorw %ax, %ax
	movw %ax, %gs   #start of memory
	movw $keyhandler, %gs:(%bx)
	movw $0x07c0, %gs:2(%bx)  #segment
	sti

	#print hh
	movb $0x68, %al
	movb $0x68, %ah
	call printw

loop:
  jmp loop

keyhandler:

	inb $0x60, %al   # get key data
	movb %al, %bl   # save it
	movb %al, %dl		#save for use (p)
	inb $0x61, %al   #keybrd control
	movb %al, %ah
	orb $0x80, %al   # disable bit 7
	outb %al, $0x61   # send it back
	xchgb %al, %ah   #get original
	outb %al, $0x61   # send that back
	movb $0x20, %al    # End of Interrupt
	outb %al, $0x20
	andb $0x80, %bl    # key released
	jnz done   #don't repeat
	movb %dl, %al		#get it back
	#scan code is in %ax
	#is %ah used at all?
	
	#pass parameter on stack
	#must be 4 bytes because of gcc
	pushl %eax
.code16gcc
	call toHex
.code16
	popl %edx	#free
	#result is in %ah,%al
	#printw prints al first
	xchgb %ah, %al

	call printw

done:
  iret

.align 256, 0

.fill 254, 1, 0

.word 0xAA55

