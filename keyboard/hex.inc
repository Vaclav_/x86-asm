	.text
	.code16gcc
toHex:
.LFB0:
	pushl	%ebp
	movl	%esp, %ebp
	subl	$20, %esp
	movl	8(%ebp), %eax
	movb	%al, -20(%ebp)
	movb	-20(%ebp), %al
	andl	$15, %eax
	movb	%al, -1(%ebp)
	movb	-20(%ebp), %al
	shrb	$4, %al
	movb	%al, -2(%ebp)
	cmpb	$9, -1(%ebp)
	ja	.L2
	addb	$48, -1(%ebp)
	jmp	.L3
.L2:
	addb	$87, -1(%ebp)
.L3:
	cmpb	$9, -2(%ebp)
	ja	.L4
	addb	$48, -2(%ebp)
	jmp	.L5
.L4:
	addb	$87, -2(%ebp)
.L5:
	xorl	%eax, %eax
	movb	-2(%ebp), %al
	movw	%ax, -4(%ebp)
	salw	$8, -4(%ebp)
	xorl	%eax, %eax
	movb	-1(%ebp), %al
	orw	%ax, -4(%ebp)
	movl	-4(%ebp), %eax
	leave
	ret

