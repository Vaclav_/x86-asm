.code16
  jmp start

.include "hex.inc"
.include "printw.inc"
.code16

start:
	xorw %ax, %ax
	movw %ax, %ds	
	movw %ax, %ss
	movw $0x9c00, %sp			#stack

movb $1, %al
shlb $2, %al
orb 	$0x80, %al

#movb 0x7c00(%si), %cl
#shlb $2, %cl
#andb $0xc0, %cl
#movw $Sector, %si
#movb 0x7c00(%si), %dl
#andb %dl, %cl



pushl %eax
.code16gcc
call toHex
.code16
popl %edx

call printw

loop:
  jmp loop

.align 256, 0
.fill 254, 1, 0
.word 0xAA55
