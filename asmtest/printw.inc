.text
.code16
#print contents of %ax
printw:
	#print bigger digit first
	xchgb %ah, %al
	pushw %ax
	movw %sp, %si
	lodsb
	movb $0x0E,%ah
	int $0x10
#second byte
	lodsb
	movb $0x0E,%ah
	int $0x10
	popw %ax
	ret
