//this function only increments the passed number
__asm__(".code16gcc");
int increment(int a) {
	a += 1;
	return a;
}
