#keyboard input
.text
.code16

jmp _start

.include "print.inc"

.code16

.globl _start
_start:
	#set the data segment address
	xorw %ax, %ax
	movw %ax, %ds

	#setup stack
	#past loaded 512 bytes
	movw %ax, %ss
	movw $0x9c00, %sp

	#C
	movl $0x43, %eax
	pushl %eax

	.code16gcc
	call increment
	.code16

	addw $0x4, %sp


	pushl %eax
	.code16gcc
	call increment
	.code16
	addw $0x4, %sp


	movb $0x41, %ah

	call printw

	# /r
	movb $0xd, %al
	# /n
	movb $0xa, %ah

	call printw

hang:
	jmp hang

#.align 256, 0
#.fill 254, 1, 0
#.byte 0x55
#.byte 0xAA



