#atamain.s
#copy next 512 bytes after boot sector from floppy
#at 0x9c00
#and jump there
.code16
  jmp start

.include "print.inc"
.code16

start:
	movw $0x07c0, %ax
	movw %ax, %ds 	#(code start), x16
	xorw %ax, %ax
	movw %ax, %ss
	movw $0x0500, %sp			#stack: free 30 kB

	#copy from floppy
	movw $DAPACK, %si                #address of "disk address packet"
	movb $0x42, %ah                #AL is unused
	movb $0x80, %dl                # drive number 0 (OR the drive # with 0x80)
	int $0x13
	
	#a
	movb $0x61, %al
	call print

	#error accessing floppy drive
	jc error
	jmp secondSector

loop:
  jmp loop

error:
	#E
	movb $0x45, %al
	call print
	jmp loop

DAPACK:
	.byte        0x10			#size of packet, always 16
	.byte        0				#always 0
blkcnt:
	.word        1				#int 13 resets this to # of blocks actually read/written
db_add: 
	.word        0x7E00     # memory buffer destination address (0:7E00 -> after first 512B)
   .word        0          # in memory page zero (segment)
d_lba:
	.long        1				#put the lba to read in this spot (lba starts at 0, so 1 is the second block)
	.long        0				# more storage bytes only for big lba's ( > 4 bytes )

.align 256, 0
.fill 254, 1, 0
.word 0xAA55
secondSector:
#S
	movb $0x53, %al
	call print
	jmp loop

#fill second block
.align 1024, 0

