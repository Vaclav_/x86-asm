#atamain.s
#copy next 512 bytes after boot sector from floppy
#at 0x7e00
#and jump there
.code16
  jmp start

.include "print.inc"
.code16

#floppy:
#512 bytes / sector
#1.44 MB
# Number of Heads (Sides) 2
# 80 Tracks (Cylinders)
# 18 Sectors per track (cyl)
# Total sectors per disk 2880
#Param
Head:				.byte 0
Sector:				.byte 2 #1+1
Track:				.byte 0

start:
	xorw %ax, %ax
	movw %ax, %ds	
	movw %ax, %ss
	movw $0x9c00, %sp			#stack

	#a
	movb $0x61, %al
	call print

	#CHS address is hardcoded
	#set AH=2
	movb $2, %ah
	#AL=sector count to be read, <128, can't cross .. boundary
	movb $1, %al
	#CH = cylinder & 0xff
	movw $Track, %si
	movb 0x7c00(%si), %ch
	#CL = Sector | ((cylinder >> 2) & 0xC0);
	#for now CL = Sector
	movb 0x7c00(%si), %cl
	shlb $2, %cl
	andb $0xc0, %cl
	movw $Sector, %si
	movb 0x7c00(%si), %dl
	orb %dl, %cl
	#DH = Head
	movw $Head, %si
	movb 0x7c00(%si), %dh
	#ES:BX -> buffer
	xorw %bx, %bx
	movw %bx, %es
	movw $0x7e00, %bx
	#Set DL = "drive number" -- typically 0x80, for the "C" drive
	#1st floppy: 0
	movb $0x0, %dl
	#int13
	#copy data from floppy
	int $0x13

	#if there is an error with the floppy, the carry flag will be set
	jc error

	movb $0x63, %al
	call print

	#jump to the copied second sector
	movw $0x7e00, %bx
	jmp *%bx
	#or:
	#jmp SecondSector

loop:
  jmp loop

error:
	#E
	movb $0x45, %al
	call print

	jmp loop


.align 256, 0
.fill 254, 1, 0
.word 0xAA55
SecondSector:
	#S
	movb $0x53, %al
	call print

loop2:
	jmp loop2

#fill second block
.align 1024, 0


