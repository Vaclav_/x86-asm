
#include <stdio.h>

const char* F_PATH = "out.bin";

int main(void) {
	FILE* fp;

	fp = fopen( F_PATH, "ab");

	if( fp == NULL ) {
		printf( "Can't open file.\n");
		return -1;
	} else
		printf( "Opened file.\n");

	long pos = ftell(fp);

	printf( "Append pos: %li\n", pos);

	char ch = 0;
	while( pos < 510 ) {
		fprintf( fp, "%c", ch );
		pos++;
	}

	ch = 0x55;
	fprintf( fp, "%c", ch);
	pos++;
	ch = 0xaa;
	fprintf( fp, "%c", ch);
	pos++;

	printf( "Final pos: %li\n", pos);

	close(fp);

	return 0;
}
