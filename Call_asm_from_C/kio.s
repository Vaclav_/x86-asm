.text
.code16
#print %al
.globl putChar
putChar:
	movw %sp, %si
	pushw %ax
	addw $4, %si
	lodsb
	movb $0x0E,%ah
	int $0x10
	popw %ax
	ret
