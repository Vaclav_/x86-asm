export PATH="$HOME/opt/cross/bin:$PATH"
i586-elf-gcc -c $1 -o $2".elf"
if [ $3 = "-p" ]; then
objdump -d $2".elf"
fi
objcopy -O binary $2".elf" $2
rm $2".elf"
echo "Done. Output: "$2

