__asm__(".code16gcc");

void putChar( char ch );

void putStr( const char* str ) {
	int i=0;
	while( str[i] ) {
		putChar(str[i]);
		i++;
	}
}

void putStrLn( const char* str ) {
	int i=0;
	while( str[i] ) {
		putChar(str[i]);
		i++;
	}
	putChar('\r');
	putChar('\n');
}

void kmain() {
	putStrLn( "Heeeeeey bro");
}
