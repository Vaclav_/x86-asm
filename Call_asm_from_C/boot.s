.text
.code16

jmp _start

.include "print.inc"

.code16

.globl _start
_start:
	#set the data segment address
	xorw %ax, %ax
	movw %ax, %ds

	#setup stack
	movw %ax, %ss
	movw $0x9c00, %sp

	#say AB
	movb $0x41, %al
	movb $0x42, %ah

	call printw

	.code16gcc
	call kmain
	.code16

hang:
	jmp hang




